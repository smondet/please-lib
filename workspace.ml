open Internal_pervasives

module Shell = struct
  type t = {command: string -> string; show_command: string -> string}

  let command_to_string_list t cmd =
    let i = Unix.open_process_in (t#shell.command cmd) in
    let rec loop acc =
      try loop (input_line i :: acc) with _ -> close_in i ; List.rev acc in
    let lines = loop [] in
    let status = Unix.close_process_in i in
    (lines, status)

  let fail_if_fail t cmd = function
    | Unix.WEXITED 0 -> ()
    | _ ->
        ksprintf failwith "%s → exited non-zero" (t#shell.show_command cmd)

  let succeed t cmd =
    let c = t#shell.command cmd in
    Unix.system c |> fail_if_fail t cmd

  let succeed_silently state cmd =
    let tmp = Filename.temp_file (sprintf "shell-command-output") ".txt" in
    try ksprintf (succeed state) "{ %s ; } > %s 2>&1" cmd tmp
    with Failure s -> ksprintf failwith "%s (%s)" s tmp

  let succeed_exec t l =
    succeed t (List.map l ~f:Filename.quote |> String.concat ~sep:" ")

  let exec_to_string_list t l =
    command_to_string_list t
      (List.map l ~f:Filename.quote |> String.concat ~sep:" ")

  let succeed_to_string_list t cmd =
    let lines, status = command_to_string_list t cmd in
    fail_if_fail t cmd status ; lines
end

module Console = struct
  type t = {message_formatter: Format.formatter; color: bool}

  let default () =
    let message_formatter = Format.err_formatter in
    let color =
      match Sys.getenv_opt "please_color" with
      | Some "true" -> true
      | Some "false" -> false
      | Some _ | None ->
          let dumb =
            try match Sys.getenv "TERM" with "dumb" | "" -> true | _ -> false
            with Not_found -> true in
          let isatty =
            try Unix.(isatty stderr) with Unix.Unix_error _ -> false in
          if (not dumb) && isatty then true else false in
    if color then (
      let bold = "\027[01m" in
      let red = "\027[31m" in
      let reset = "\027[m" in
      let color_of_tag = function
        | "prompt" -> Some bold
        | "shout" -> Some red
        | _ -> None in
      Format.(
        pp_set_formatter_tag_functions message_formatter
          { mark_open_tag= (fun _ -> "")
          ; mark_close_tag= (fun _ -> "")
          ; print_open_tag=
              (fun tag ->
                match color_of_tag tag with
                | Some c -> fprintf message_formatter "%s" c
                | None -> ())
          ; print_close_tag=
              (fun tag ->
                if color_of_tag tag <> None then
                  fprintf message_formatter "%s" reset) } ;
        pp_set_tags message_formatter true) ) ;
    {message_formatter; color}

  module Make_message (P : sig
    val prefix : string
  end) =
  struct
    let say state fmt =
      let open Format in
      let ppf = state#console.message_formatter in
      kasprintf
        (fun s ->
          let prompt = sprintf ">>> %s: " P.prefix in
          pp_open_box ppf (String.length prompt) ;
          pp_open_tag ppf "prompt" ;
          pp_print_string ppf prompt ;
          pp_close_tag ppf () ;
          pp_print_text ppf s ;
          pp_close_box ppf () ;
          pp_print_newline ppf ())
        fmt
  end
end

module Git_remote = struct
  module Msg = Console.Make_message (struct let prefix = "Git-Remote" end)

  type t = {uri: string; name: string}

  let make ~name uri = {name; uri}
  let gist ~name id = make ~name (sprintf "git@gist.github.com:%s.git" id)

  let ensure state t =
    let open Febusy.Edsl in
    phony (sprintf "remote-%s" t.name)
    <-- fun () ->
    let remotes () =
      Shell.succeed_to_string_list state "git remote -v"
      |> List.filter ~f:(fun s -> String.is_prefix s ~prefix:(t.name ^ "\t"))
    in
    match remotes () with
    | [] -> Shell.succeed_exec state ["git"; "remote"; "add"; t.name; t.uri]
    | _ -> Msg.say state "%s already set" t.name
end

module Subtree = struct
  type t =
    { name: string
    ; remote: Git_remote.t
    ; branch: string
    ; description: string
    ; prefix: string
    ; squashed: bool
    ; witness: string }

  let make ?(squashed = false) name ~remote ~branch ~prefix ~witness
      description =
    {name; remote; branch; squashed; description; prefix; witness}

  module Msg = Console.Make_message (struct let prefix = "Sub-Tree" end)

  let name s = s.name

  let ensure ?commit_message state t =
    let open Febusy.Edsl in
    Msg.say state "Ensuring %s → %s" t.name t.prefix ;
    Git_remote.ensure state t.remote
    >>= fun _ _ ->
    File.make (t.prefix // t.witness) (fun f ->
        (* This is a bit redundant, we should make a special
           “file-exists” artifact instead. *)
        if Sys.file_exists f then Msg.say state "Witness exists: %s" f
        else
          Shell.succeed_exec state
            ( ["git"; "subtree"; "add"]
            @ (if t.squashed then ["--squash"] else [])
            @ Option.value_map commit_message ~default:[] ~f:(fun m ->
                  ["--message"; m])
            @ [ sprintf "--prefix=%s" t.prefix
              ; t.remote.Git_remote.name; t.branch ] ))

  let fetch_if state t fetch =
    if fetch then (
      Msg.say state "Fetching %S" t.remote.name ;
      Shell.succeed_silently state
        (sprintf "git fetch %s %s " t.remote.name t.branch) )

  let log ?(fetch = true) state t =
    fetch_if state t fetch ;
    let split_commit =
      Shell.succeed_to_string_list state
        (sprintf "git subtree split --prefix %s" (Filename.quote t.prefix))
      |> String.concat ~sep:"" in
    Msg.say state "Log for `%s` (Vs `%s`)" t.name
      ( String.sub split_commit ~index:0 ~length:8
      |> Option.value ~default:"NO-SPLIT-COMMIT" ) ;
    Shell.succeed_exec state
      [ "git"; "log"; "--reverse"; "--oneline"
      ; sprintf "%s/%s..%s" t.remote.name t.branch split_commit ]

  let push_or_pull ?branch state t title args =
    let br = Option.value branch ~default:t.branch in
    Msg.say state "%s `%s` → `%s/%s`" title t.name t.remote.name br ;
    Shell.succeed_exec state
      ( ["git"; "subtree"] @ args
      @ (if t.squashed then ["--squash"] else [])
      @ ["--prefix"; t.prefix; t.remote.name; br] )

  let push ?branch state t = push_or_pull ?branch state t "Push" ["push"]

  let pull ?branch state t =
    let ci_msg =
      sprintf "Merge %s/%s (./%s/)" t.remote.name t.branch t.prefix in
    push_or_pull ?branch state t "Pull" ["pull"; "-m"; ci_msg]

  let of_prefix_exn subtrees prefix =
    match
      List.filter subtrees ~f:(fun s -> String.is_prefix s.name ~prefix)
    with
    | [] -> ksprintf failwith "%S not found\n%!" prefix
    | [one] -> one
    | more ->
        ksprintf failwith "%S is not disciminatory enough: %s\n%!" prefix
          (String.concat ~sep:", " (List.map ~f:(fun s -> s.name) more))

  let diff ?(fetch = true) state t =
    fetch_if state t fetch ;
    let cmd =
      sprintf "git diff %s/%s HEAD:%s/" t.remote.name t.branch t.prefix in
    Msg.say state "Diffing with `%s`" cmd ;
    Shell.succeed state cmd

  let repo_not_dirty state =
    match Shell.succeed_to_string_list state "git diff --name-only HEAD" with
    | [] -> ()
    | more ->
        ksprintf failwith "Repository dirty: %s" (String.concat ~sep:", " more)

  let rm ?commit_message state t =
    let safe_path = "." // t.prefix // "" in
    repo_not_dirty state ;
    Msg.say state "Git-removing: %S" safe_path ;
    Shell.succeed_silently state
      (sprintf "git rm -r %s" (Filename.quote safe_path)) ;
    Msg.say state "Removing remaining dust from %S" safe_path ;
    Shell.succeed_silently state
      (sprintf "rm -fr %s" (Filename.quote safe_path)) ;
    Msg.say state "Committing removal of %S" safe_path ;
    let msg =
      let default = sprintf "Remove subtree %s (%s)" t.name t.prefix in
      Option.value commit_message ~default in
    Shell.succeed_silently state
      (sprintf "git commit -m %s" (Filename.quote msg)) ;
    ()
end

module Opam_switch = struct
  module Msg = Console.Make_message (struct let prefix = "Opam-Switch" end)

  type t =
    | Named of
        { name: string
        ; ocaml_version: string
        ; build: unit -> string list (* script *) }

  let named name ~ocaml_version build = Named {name; ocaml_version; build}
  let name = function Named {name; _} -> name

  let setup_commands (Named {name; _}) =
    [ sprintf "export OPAMSWITCH=%S" name
    ; "export OPAMYES=1"; "eval $(opam env)" ]

  let ensure state = function
    | Named {name; ocaml_version; build} as opsw ->
        let open Febusy.Edsl in
        phony (sprintf "opam-switch-%s-create" name)
        <-- (fun () ->
              let all =
                Shell.succeed_to_string_list state "opam switch list --short"
              in
              if List.mem ~set:all name then
                Msg.say state "Opam switch %S: Already there" name
              else (
                Msg.say state "Creating %S" name ;
                ksprintf
                  (Shell.succeed_silently state)
                  "opam switch create %s %s" name ocaml_version ))
        >>= fun _ () ->
        phony (sprintf "opam-switch-%s-build" name)
        <-- fun () ->
        let cmds = setup_commands opsw @ build () in
        Msg.say state "running %s's build commands" name ;
        Shell.succeed_silently state (String.concat ~sep:" ; " cmds)
end

module Duniverse = struct
  module Msg = Console.Make_message (struct let prefix = "Duniverse" end)

  type t =
    { name: string
    ; opam_switch: Opam_switch.t option
    ; dirs: string list
    ; build_dir: string
    ; build_targets: string list
    ; disable_dirs: string list
    ; extra_env: (string * string) list }

  let make ?(disable_dirs = []) ?opam_switch ?build_dir ?(env = []) name ~dirs
      ~targets =
    { name
    ; opam_switch
    ; build_dir= Option.value build_dir ~default:(sprintf "_build_%s" name)
    ; dirs
    ; build_targets= targets
    ; disable_dirs
    ; extra_env= env }

  let run_dune ?(clean_dune_files = true) ?(profile = "dev") state t cmd
      more_args =
    let open Febusy.Edsl
    (* just for System *) in
    Msg.say state "Setting up %S" t.name ;
    System.write_lines "dune-project" ["(lang dune 1.6)"] ;
    let hierarchy =
      List.map t.dirs ~f:(fun dir ->
          String.split ~on:(`Character '/') dir |> List.filter ~f:(( <> ) ""))
    in
    let dunes_to_rm = ref [] in
    let write_dune path dirs =
      let content = String.concat ~sep:" " dirs in
      let dune = path // "dune" in
      if not (Sys.file_exists dune) then dunes_to_rm := dune :: !dunes_to_rm ;
      let first_line = ";; duniverse generated by please-framework" in
      Msg.say state "|-- %s/dune -> %s" path content ;
      if
        Sys.file_exists dune
        && List.hd (System.read_lines dune) <> Some first_line
      then ksprintf failwith "I don't want to overwrite %S" dune ;
      System.write_lines dune [first_line; sprintf "(dirs %s)" content] in
    let db = Hashtbl.create 42 in
    let rec loop path h =
      match h with
      | [] -> ()
      | one :: more ->
          Hashtbl.add db path one ;
          (* db := (path, List.concat_map h ~f:(function [] -> [] | one :: _ -> [one])) :: !db; *)
          loop (path // one) more in
    List.iter hierarchy ~f:(loop ".") ;
    let keys = Hashtbl.fold (fun k _ p -> k :: p) db [] in
    List.iter keys ~f:(fun k -> write_dune k (Hashtbl.find_all db k)) ;
    List.iter t.disable_dirs ~f:(fun d ->
        let dir = Filename.basename d in
        let location = Filename.dirname d // "dune" in
        Msg.say state "|-- patching %s (disable %s)" location dir ;
        let current =
          if Sys.file_exists location then System.read_lines location else []
        in
        let line = sprintf "(data_only_dirs %s)" dir in
        let lines = List.filter current ~f:(( <> ) line) in
        System.write_lines location (line :: lines)) ;
    let cmds =
      let args l = String.concat ~sep:" " (List.map ~f:Filename.quote l) in
      Option.value_map ~default:[] ~f:Opam_switch.setup_commands t.opam_switch
      @ List.map t.extra_env ~f:(fun (k, v) ->
            sprintf "export %s=%s" k (Filename.quote v))
      @ [ sprintf "dune %s --profile %s --build-dir %s --root=$PWD %s" cmd
            profile t.build_dir (args more_args) ]
      @
      if clean_dune_files && !dunes_to_rm <> [] then
        [sprintf "rm -f %s" (String.concat ~sep:" " !dunes_to_rm)]
      else [] in
    (* List.iter cmds
     *   ~f:(eprintf "CMD(%s): %s\n%!" (String.concat ~sep:" " !dunes_to_rm)) ; *)
    Shell.succeed state (String.concat ~sep:" && " cmds)

  let build ?clean_dune_files ?(extra_targets = [])
      ?(only_extra_targets = false) ?profile state t =
    run_dune state t "build" ?profile ?clean_dune_files
      ((if only_extra_targets then [] else t.build_targets) @ extra_targets)

  let env t =
    let v n s = [(n, s)] in
    let opt n f = function None -> [] | Some s -> v n (f s) in
    let list n l = v n (String.concat ~sep:" " l) in
    v "duniverse" t.name
    @ opt "OPAMSWITCH" Opam_switch.name t.opam_switch
    @ list "duniverse_dirs" t.dirs
    @ v "duniverse_build_dir" t.build_dir
    @ list "duniverse_build_targets" t.build_targets
    @ t.extra_env
end

module File = struct
  type t = {path: string; content: string list; no_clean: bool}

  module Msg = Console.Make_message (struct let prefix = "Workspace-File" end)

  let make ?(no_clean = false) path content = {path; content; no_clean}
  let file = make
  let repo_file = make ~no_clean:true
  let path f = f.path

  let write f =
    System.cmdf ~silent:true "mkdir -p %s" Filename.(dirname f.path |> quote) ;
    System.write_lines f.path f.content ;
    ()

  let ensure state f =
    let open Febusy.Edsl in
    return_value (sprintf "content-of-%s" f.path) f.content
    >>= fun _ _ ->
    File.make f.path (fun _ ->
        Msg.say state "writing `%s`." f.path ;
        write f)
end

module Git_ignore = struct
  type t = File.t

  let make ?(duniverses = []) ?(path = ".gitignore") more_lines =
    File.make path
    @@ List.concat_map duniverses ~f:(fun {Duniverse.build_dir; _} ->
           [build_dir])
    @ more_lines
end
