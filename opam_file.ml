open Internal_pervasives

type qualifier =
  [`Build | `Version of [`GT] * string | `And of qualifier * qualifier]

let qualifier_to_string q =
  let rec go = function
    | `And (a, b) -> sprintf "%s & %s" (go a) (go b)
    | `Build -> "build"
    | `Version (`GT, s) -> sprintf ">= %S" s
  in
  sprintf "{%s}" (go q)

let dep ?qualify ?(build = false) n =
  sprintf "%S%s" n
    ( match (qualify, build) with
    | None, false -> ""
    | None, true -> " " ^ qualifier_to_string `Build
    | Some q, false -> " " ^ qualifier_to_string q
    | Some q, true -> " " ^ qualifier_to_string (`And (`Build, q)) )

let obvious_deps = [dep "jbuilder" ~build:true; dep "ocamlfind" ~build:true]

let make ?(opam_version = "1.2") ~maintainer ?authors ?(deps = obvious_deps)
    ~homepage ?bug_reports ?dev_repo ?(license = "ISC") ?version
    ?(ocaml_min_version = "4.03.0") ?(configure_script = "please.mlt") name =
  let string k v = sprintf "%s: %S" k v in
  let opt_default o d = match o with None -> d | Some s -> s in
  let opt_f f k v = match v with None -> [] | Some s -> [f k s] in
  (let opt_string k v = opt_f string k v in
   [ sprintf "# This Opam file was auto-generated, see the `%s` script."
       configure_script
   ; string "opam-version" opam_version
   ; string "maintainer" maintainer
   ; (match authors with None -> [maintainer] | Some l -> l)
     |> List.map ~f:(sprintf "%S")
     |> String.concat ~sep:"\n  "
     |> sprintf "authors: [\n  %s\n]"
   ; string "homepage" homepage
   ; string "bug-reports" (opt_default bug_reports (homepage // "issues"))
   ; string "dev-repo" (opt_default dev_repo (homepage ^ ".git"))
   ; string "license" license ]
   @ opt_string "version" version)
  @ [ sprintf "available: [ ocaml-version >= %S ]" ocaml_min_version
    ; sprintf "build: [\n   [ \"dune\" \"build\" \"-p\" %s \"-j\" jobs ]\n]"
        name
    ; sprintf "depends: [\n%s\n]"
        (List.map ~f:(sprintf "  %s") deps |> String.concat ~sep:"\n") ]

let v2 ~maintainer ?authors ?(deps = obvious_deps) ~homepage ?bug_reports
    ?dev_repo ?(license = "ISC") ?version ?(ocaml_min_version = "4.03.0")
    ?(header = []) ?synopsis ?description name =
  let string k v = sprintf "%s: %S" k v in
  let opt_default o d = match o with None -> d | Some s -> s in
  let opt_f f k v = match v with None -> [] | Some s -> [f k s] in
  (let opt_string k v = opt_f string k v in
   [sprintf "# This Opam file was auto-generated."]
   @ List.map header ~f:(sprintf "# %s")
   @ [ string "opam-version" "2.0"
     ; string "maintainer" maintainer
     ; (match authors with None -> [maintainer] | Some l -> l)
       |> List.map ~f:(sprintf "%S")
       |> String.concat ~sep:"\n  "
       |> sprintf "authors: [\n  %s\n]"
     ; string "homepage" homepage
     ; string "bug-reports" (opt_default bug_reports (homepage // "issues"))
     ; string "dev-repo" (opt_default dev_repo ("git+" ^ homepage ^ ".git"))
     ; string "license" license ]
   @ opt_string "version" version)
  @ [ sprintf "build: [\n   [ \"dune\" \"build\" \"-p\" %S \"-j\" jobs ]\n]"
        name
    ; sprintf "depends: [\n%s\n]"
        ( sprintf "  \"ocaml\" { >= %S }" ocaml_min_version
          :: List.map ~f:(sprintf "  %s") deps
        |> String.concat ~sep:"\n" ) ]
  @ opt_f (sprintf "%s: %S") "synopsis" synopsis
  @ opt_f
      (fun k s -> sprintf "%s: \"\"\"\n%s\n\"\"\"" k (String.strip s))
      "description" description
