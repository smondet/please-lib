Please-lib
==========

“Please” is a library used to build little command-line apps to manage large
OCaml [monorepos](https://en.wikipedia.org/wiki/Monorepo) composed of various
smaller projects using
[git-subtrees](https://manpages.debian.org/testing/git-man/git-subtree.1.en.html)
and [Dune](https://dune.build/)'s “composability.”


This is **work-in-progress**, but used every day, the current “model” is:

- You have one (often private) “work” monorepo.
- One of its subtrees is the `please-lib` repository.
- You have one local directory (e.g. `./tools/`) with your implementation of the
  monorepo management tool (e.g. `./tools/please.ml`).
    - This app defines the subtrees, the “duniverses,” and their opam-switches.
    - It instanciates the [Cmdliner](https://erratique.ch/software/cmdliner)
      commands provided by the library (create/destroy/pull/push
      subtrees/opam-switches, build the duniverses, …).
    - You can also add your own commands.
- A build shell-script builds the manager which then properly configures and
  calls `dune` on the various independent duniverses.


How To Use
----------

### Initialize Monorepo

Let's build a monorepo from scratch:

For instance within a `docker run -it -v $PWD:/please ocaml/opam2:4.06`:

Create a project with a `README.md` on your favorite forge, or

     mkdir my-project; cd my-project ; git init . 
     echo 'my-project' > README.md ; git add README.md ; git commit -m 'Add README'

Use [`initialize.sh`](./initialize.sh) from this repository:

    curl -O https://gitlab.com/smondet/please-lib/raw/master/initialize.sh
    sh initialize.sh
    
The subtree has been added, and the files `tools/please.ml` and `mk` have been
created (but not committed, that's up to you).  `./mk` builds `./please` and can
call `./please build`, see `./please --help` to see what is already available,
and start modifying `tools/please.ml` to add your own setup.

The script [`initialize.sh`](./initialize.sh) acccepts a few options as
environment variables, see `sh initialize.sh --help`.

### Add Things: TODO

🚧 👷 Work in Progress 👷 🚧

Examples
--------

Some repos that use it:

- <https://github.com/hammerlab/genspio>
- <https://gitlab.com/smondet/vecosek>
- <https://gitlab.com/smondet/stamifi>
- <https://gitlab.com/smondet/genspio-web-demo>

