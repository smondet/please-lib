open Internal_pervasives

let make_and_exit ?(force_rebuild = false) ~on_value ~name thing =
  let open Febusy.Edsl in
  let state_file = sprintf "/tmp/%s.state" name in
  if force_rebuild then System.cmdf "rm -f %s" state_file ;
  let res, log = Make_unix.run ~state_file thing in
  let ret =
    match res with
    | Ok {value; _} -> on_value value ; 0
    | Error e ->
        eprintf "Build ERROR: %s\n%!" (Febusy.Common.Error.to_string e) ;
        1 in
  let logfile = sprintf "/tmp/%s.log" name in
  System.write_lines logfile [log] ;
  if ret <> 0 then eprintf "Build-Log: %s\n%!" logfile ;
  Pervasives.exit ret

let febusy_command cmdname ~docs ~doc term =
  let open Cmdliner in
  let open Term in
  ( pure (fun feb -> make_and_exit ~name:cmdname feb ~on_value:(fun _ -> ()))
    $ term
  , info cmdname ~doc ~docs )

let unit_command cmdname ~docs ~doc f =
  let open Cmdliner in
  let open Term in
  ( pure (fun f () ->
        try (f () : unit) with
        | Failure s -> eprintf "ERROR: %s\n%!" s ; Pervasives.exit 3
        | other -> raise other)
    $ f $ pure ()
  , info cmdname ~doc ~docs )

let make commands =
  let open Cmdliner in
  let help =
    Term.
      ( ret (pure (`Help (`Auto, None)))
      , info "please" ~doc:"Git/workspace/duniverse management." ) in
  Term.exit @@ Term.eval_choice (help : unit Term.t * _) commands

let build_state
    ?(shell_command = fun s -> sprintf "sh -c %s" (Filename.quote s))
    ?(show_shell_command = sprintf "%S") () =
  let open Workspace in
  let shell =
    Shell.{command= shell_command; show_command= show_shell_command} in
  let console = Console.default () in
  let offline = try Sys.getenv "please_offline" = "true" with _ -> false in
  object
    method shell = shell

    method console = console

    method offline = offline
  end

(* Now the more "particular" ones *)

open Workspace

let shell ?(name = "shell") state =
  let open Cmdliner in
  unit_command name ~doc:"Run a shell command in the state's shell"
    ~docs:"SHELL"
    Term.(
      pure (fun command_list args () ->
          match command_list with
          | false -> Shell.succeed_exec state args
          | true -> Shell.succeed state (String.concat ~sep:" ; " args))
      $ Arg.(
          value
            (flag
               (info ["C"; "command-list"]
                  ~doc:"Treat arguments as a list of shell commands.")))
      $ Arg.(
          value
            (pos_all string [] (info [] ~docv:"ARGS" ~doc:"Exec arguments"))))

let ensure_all ?(name = "ensure-all") ~subtrees ~opam_switches ?(files = [])
    state =
  let open Cmdliner in
  febusy_command name ~doc:"Ensure workspaces, opam-switches and more?"
    ~docs:"COMMON"
  @@ Term.(
       pure
         Febusy.Edsl.(
           fun only_files filters () ->
             let filter_thing name =
               match filters with
               | [] -> true
               | more ->
                   List.exists more ~f:(fun prefix ->
                       String.is_prefix name ~prefix) in
             let ensure_filtered_list l name ensure () =
               List.filter_map l ~f:(fun n ->
                   if filter_thing (name n) then Some (ensure state n)
                   else None)
               |> join in
             let unless_only_files f = if only_files then join [] else f () in
             let subs =
               unless_only_files
               @@ ensure_filtered_list subtrees Subtree.name Subtree.ensure
             in
             let oswitches =
               unless_only_files
               @@ ensure_filtered_list opam_switches Opam_switch.name
                    Opam_switch.ensure in
             let files =
               ensure_filtered_list files Workspace.File.path
                 Workspace.File.ensure () in
             subs =<>= oswitches >>= fun _ _ -> files)
       $ Arg.(value (flag (info ["files"] ~doc:"Consider only files")))
       $ Arg.(
           let doc =
             sprintf "Filter things to ensure, Subrees: %s, Opam-switches: %s."
               ( List.map subtrees ~f:(fun s -> s.Subtree.name)
               |> String.concat ~sep:"," )
               ( List.map opam_switches ~f:Opam_switch.name
               |> String.concat ~sep:"," ) in
           value (pos_all string [] (info [] ~docv:"FILTER-NAME" ~doc))))

let subtree_defaults ?(prefix_commands = "") state ~subtrees =
  let cmd n = sprintf "%s%s" prefix_commands n in
  let open Cmdliner in
  let fetch_term () =
    let open Term in
    Arg.(
      match state#offline with
      | false ->
          pure (fun n -> `Fetch (not n))
          $ value
              (flag (info ["no-fetch"] ~doc:"Do not call fetch before log."))
      | true ->
          pure (fun n -> `Fetch n)
          $ value (flag (info ["fetch"] ~doc:"Do call fetch before log.")))
  in
  let required_subtree_term ~position () =
    let open Term in
    let doc =
      sprintf "Specific subtree (%s)."
        (String.concat ~sep:", " (List.map ~f:Subtree.name subtrees)) in
    Arg.(
      pure (fun s -> Subtree.of_prefix_exn subtrees s)
      $ required
          (pos position (some string) None (info [] ~docv:"SUBTREE" ~doc)))
  in
  let docs = "SUBTREES" in
  let unit_command = unit_command ~docs in
  [ unit_command (cmd "list") ~doc:"List subtrees"
      Cmdliner.(
        let open Term in
        pure (fun () ->
            let open Subtree in
            let cmp a b = String.compare a.prefix b.prefix in
            let subs = List.sort ~cmp subtrees in
            let longest =
              List.fold subs ~init:0 ~f:(fun prev {prefix; _} ->
                  max prev (String.length prefix)) in
            List.iter subs ~f:(fun s ->
                let pad =
                  String.make (2 + longest - String.length s.prefix) '.' in
                let there = Sys.file_exists (s.prefix // s.witness) in
                printf "* %s%s: %s%s.\n%!" s.prefix pad s.description
                  (if there then "" else " (ABSENT)"))))
  ; unit_command (cmd "log") ~doc:"Get the changes in subtrees."
      Cmdliner.(
        let open Term in
        pure (fun (`Fetch fetch) args () ->
            match args with
            | [] -> List.iter subtrees ~f:(Subtree.log ~fetch state)
            | some ->
                List.iter some ~f:(fun prefix ->
                    let one = Subtree.of_prefix_exn subtrees prefix in
                    Subtree.log state ~fetch one))
        $ fetch_term ()
        $ Arg.(
            let doc =
              sprintf "Restrict to a list of subtrees (%s)."
                (String.concat ~sep:", " (List.map ~f:Subtree.name subtrees))
            in
            value (pos_all string [] (info [] ~doc ~docv:"SUBTREES"))))
  ; unit_command (cmd "diff") ~doc:"Call diff on the subtree"
      Term.(
        pure (fun (`Fetch fetch) subt () -> Subtree.diff state ~fetch subt)
        $ fetch_term ()
        $ required_subtree_term ~position:0 ())
  ; unit_command (cmd "rm") ~doc:"Remove a subtree."
      Cmdliner.(
        let open Term in
        pure (fun one () -> Subtree.rm state one)
        $ required_subtree_term ~position:0 ())
  ; febusy_command "remake" ~doc:"Remove and Re-Ensure subtrees" ~docs
      Term.(
        pure (fun rm_message add_message one () ->
            Subtree.rm ?commit_message:rm_message state one ;
            Subtree.ensure ?commit_message:add_message state one)
        $ Arg.(value (opt (some string) None (info ["rm-message"])))
        $ Arg.(value (opt (some string) None (info ["add-message"])))
        $ required_subtree_term ~position:0 ()) ]
  @
  let push_pull ?(with_fresh = false) c ~doc f_pullsh =
    unit_command (cmd c) ~doc
      Cmdliner.(
        let open Term in
        let fresh_branch name =
          sprintf "%s-%s" name
            ( System.cmd_to_string_list "date +'%Y%m%d-%H%M%S'"
            |> String.concat ~sep:"" ) in
        pure (fun one fresh branch () ->
            let branch =
              if fresh then Some (fresh_branch one.Subtree.name) else branch
            in
            f_pullsh ?branch state one)
        $ required_subtree_term ~position:0 ()
        $ ( if with_fresh then
            Arg.(
              value
                (flag
                   (info ["fresh"]
                      ~doc:
                        (sprintf "Create a fresh branch name (e.g. `%s`)."
                           (fresh_branch "<subtreename>")))))
          else pure false )
        $ Arg.(
            value
              (pos 1 (some string) None
                 (info [] ~docv:"BRANCH" ~doc:"Specific branch.")))) in
  [ push_pull "push" ~doc:"Push the changes from a subtree." Subtree.push
      ~with_fresh:true
  ; push_pull "pull" ~doc:"Pull potential changes into a subtree." Subtree.pull
  ]

let duniverse_defaults ?(prefix_commands = "") state ~duniverses =
  let open Cmdliner in
  let duniverse_selection () =
    Arg.(
      let select_duns =
        List.map duniverses ~f:(fun d -> (d.Duniverse.name, d)) in
      let doc =
        sprintf "Select duniverse (default: ALL: %s)."
          (String.concat ~sep:", " (List.map ~f:fst select_duns)) in
      value
        (opt_all (enum select_duns) duniverses
           (info ["duniverse"] ~docv:"NAME" ~doc))) in
  let cmd n = sprintf "%s%s" prefix_commands n in
  let docs = "DUNIVERSE" in
  let unit_command = unit_command ~docs in
  [ unit_command (cmd "build") ~doc:"Build duniverses."
      (let open Term in
      pure (fun duniverses only_extra_targets keep_dunes extra_targets () ->
          List.iter duniverses ~f:(fun dun ->
              Duniverse.build ~only_extra_targets ~extra_targets
                ~clean_dune_files:(not keep_dunes) state dun))
      $ duniverse_selection ()
      $ Arg.(
          value
            (flag
               (info ["only-extra-targets"]
                  ~doc:"Do not run the default targets.")))
      $ Arg.(
          value
            (flag
               (info ["keep-dune-files"]
                  ~doc:"Do not clean-up the generated dune files.")))
      $ Arg.(
          value
            (pos_all string []
               (info [] ~docv:"EXTRA-TARGETS" ~doc:"Extra targets"))))
  ; unit_command (cmd "clean") ~doc:"Clean duniverses."
      Term.(
        pure (fun duniverses () ->
            List.iter duniverses ~f:(fun dun ->
                ksprintf
                  (Shell.succeed_silently state)
                  "rm -fr %s" dun.Duniverse.build_dir))
        $ duniverse_selection ())
  ; unit_command (cmd "dune") ~doc:"Call dune for a list of duniverses."
      Term.(
        pure (fun duniverses cmd args () ->
            List.iter duniverses ~f:(fun dun ->
                Duniverse.run_dune state dun cmd args))
        $ Arg.(
            let select_duns = List.map duniverses ~f:(fun d -> (d.name, d)) in
            let doc =
              sprintf "Select duniverses (%s)."
                (String.concat ~sep:", " (List.map ~f:fst select_duns)) in
            required
              (pos 0
                 (some (list ~sep:',' (enum select_duns)))
                 None
                 (info [] ~docv:"CSV-NAMES" ~doc)))
        $ Arg.(
            required
              (pos 1 (some string) None
                 (info [] ~docv:"CMD" ~doc:"The Dune command.")))
        $ Arg.(
            value
              (pos_right 1 string []
                 (info [] ~docv:"EXTRA-ARGUMENTS" ~doc:"Extra arguments."))))
  ; unit_command (cmd "env")
      ~doc:"Print environment variables for a duniverse."
      Term.(
        pure (fun duniverse () ->
            let env = Duniverse.env duniverse in
            List.iter env ~f:(fun (k, v) ->
                printf "export %s=%s\n%!" k (Filename.quote v)))
        $ Arg.(
            let select_duns = List.map duniverses ~f:(fun d -> (d.name, d)) in
            let doc =
              sprintf "Select duniverse (from %s)."
                (String.concat ~sep:", " (List.map ~f:fst select_duns)) in
            required
              (pos 0 (some (enum select_duns)) None (info [] ~docv:"NAME" ~doc))))
  ; unit_command (cmd "fmt") ~doc:"Call OCamlFormat."
      Term.(
        pure (fun duniverse () ->
            let open Duniverse in
            let dirs = duniverse.dirs in
            let cmd =
              sprintf
                "eval $(opam env --switch %s) ; find %s -type f \\( -name \
                 '*.ml' -o -name '*.mli' -o -name '*.mlt' \\) -print "
                ( Option.map ~f:Opam_switch.name duniverse.opam_switch
                |> Option.value_exn
                     ~msg:"Duniverse does not define an opam switch." )
                (String.concat ~sep:" " dirs) in
            let files = Shell.succeed_to_string_list state cmd in
            List.iteri files ~f:(fun idx path ->
                if Sys.file_exists (Filename.dirname path // ".ocamlformat")
                then (
                  let cmd =
                    sprintf
                      "eval $(opam env --switch %s 2> /dev/null) ; \
                       ocamlformat --inplace %s || echo continuing "
                      ( Option.map ~f:Opam_switch.name duniverse.opam_switch
                      |> Option.value_exn
                           ~msg:"Duniverse does not define an opam switch." )
                      (Filename.quote path) in
                  eprintf "[% 6d] Formatting %s\n%!" idx path ;
                  Shell.succeed state cmd )
                else eprintf "[% 6d] Ignoring %s\n%!" idx path))
        $ Arg.(
            let select_duns = List.map duniverses ~f:(fun d -> (d.name, d)) in
            let doc =
              sprintf "Select duniverse (from %s)."
                (String.concat ~sep:", " (List.map ~f:fst select_duns)) in
            required
              (pos 0 (some (enum select_duns)) None (info [] ~docv:"NAME" ~doc))))
  ]
