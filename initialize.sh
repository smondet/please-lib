#! /bin/sh

set -e

say () {
    echo "Plz>> $*" >&2
}
fail () {
    say "ERROR: $*"
    exit 3
}

usage () {
    cat <<EOF
Usage : [switch=<opam-switch>] [install_deps=false] [...] $0

The following environment variables can be used:

- 'switch': set the opam-switch to build please itself
- 'install_deps' (default: true): at the end of initialization,
  install please's dependencies.
- 'please_remote': set the git-remote of the local please-lib subtree
  (defaults to Gitlab).
- 'make_repo=TITLE': setup the repository from scratch (experimental).

EOF
}

case "$1" in
    "help" | "--help" | "-h" | "-help" )
        usage
        exit 0 ;;
    "" )  ;;
    * )
        say "Wrong command line '$*'"
        usage
        exit 2 ;;
esac


if [ -f ./.git ] || [ -d ./.git ] ; then
    say "OK: git"
else
    if [ "$make_repo" = "" ] ; then
        fail "KO: please, do the git init and add a commit yourself (or use 'make_repo=...')"
    else
        git init .
        echo "$make_repo" >> README.md
        echo "$make_repo" | sed 's/./=/g' >> README.md
        git add README.md
        git commit -m 'Initialize README.md'
    fi
fi

if [ "$force_clean" = "true" ] ; then
    say "Force-clean: deleting ./mk tools/please.ml tools/dune"
    rm -f ./mk tools/please.ml tools/dune
fi

please_remote=${remote:-https://gitlab.com/smondet/please-lib.git}

if [ -f please-lib/README.md ] ; then
    say "Please-lib seems already there"
else
    git remote add please-lib-remote $please_remote || say "Continuing"
    git subtree add --prefix=please-lib/ please-lib-remote master
fi
git subtree pull --prefix=please-lib/ please-lib-remote master

script=mk
if [ -f ./mk ] ; then
    script=$(mktemp --tmpdir=. -t mk-XXX.sh)
    say "There is an 'mk' script already, writing to $script"
fi

opam_switch=${switch:-4.07.1}
cat > $script <<EOF
#! /bin/sh

OPAMSWITCH=$opam_switch eval \$(opam env)
please-lib/build please-lib tools || exit 22

# Now it's your turn...

selftest () {
    ./please ensure please-lib  # Making sure please-lib subtree below works...
}

build () {
    set -e
    echo "Build-Start: \$(date -R)"
    ./please build
    echo "Build-End: \$(date -R)"
}

# Main with some editing protection attempt
# https://stackoverflow.com/questions/8335747/emacs-workflow-to-edit-bash-scripts-while-they-run/8926090#8926090
{
    if [ "\$1" = "" ] ; then
        build
        exit
    else
        \$*
        exit
    fi
}

EOF

chmod +x $script

pleasedir=tools/
if [ -f $pleasedir/please.ml ] ; then
    pleasedir=$(mktemp --tmpdir=. -d tools_XXX)
    say "Not touching tools/, using $pleasedir"
fi

mkdir -p $pleasedir

cat > $pleasedir/please.ml <<EOF
open Please_framework
open Internal_pervasives
open Please_framework.Workspace
module System = Febusy.Edsl.System

module Msg = Console.Make_message (struct
  let prefix = "Plz"
end)


let plzmod_subtree =
  let name = "please-lib" in
  Subtree.make name "The Please-lib workspace backported" ~prefix:"please-lib"
    ~remote:Git_remote.(make ~name:"please-lib-remote" "$please_remote")
    ~branch:"master" ~witness:"README.md"

let opam_switches = []
let subtrees = [ plzmod_subtree ]
let duniverses = [
  Duniverse.make "demo" ~dirs:[] ~targets:[]
]

let files =
  [ Git_ignore.make ~path:".gitignore" ~duniverses
      [ "tools/.merlin"
      ; sprintf "_build_please"
      ; "please"
      ; ".merlin"
      ; "dune-project"] ]

let () =
  let open Main in
  let state = build_state () in
  let cmds =
    [ensure_all state ~subtrees ~opam_switches ~files]
    @ duniverse_defaults state ~duniverses
    @ subtree_defaults state ~subtrees
    @ [shell state]
  in
  make cmds

EOF
cat > $pleasedir/dune <<EOF
(executables
  (names please)
  ;;(public_names please)
  (libraries please-framework))
EOF

if [ "install_deps" != "false" ] ; then
    say "Getting dependencies"
    (
        OPAMSWITCH=$opam_switch eval $(opam env)
        opam update
        opam install --yes febusy cmdliner
    )
fi

say "Running selftest"
./$script selftest



